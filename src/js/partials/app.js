$(window).on('load', function () {
    setTimeout(() => {
        $('body').removeClass('is-preload');
    }, 1000);
});

$(document).ready(function () {
    init();
});

$(window).resize(function () {
    init();
});

function init() {

    const $body = $('body');
    const $select = $('.select');
    const $btnMenu = $('#btnMenu');
    const $btnMenuClose = $('#btnMenuClose');

    $btnMenu.on('click', function () {
        $body.addClass('is-menu-open');
    });

    $btnMenuClose.on('click', function () {
        $body.removeClass('is-menu-open');
    });

    if ($select.length) {
        $select.niceSelect();

        $('.nice-select').on('click', function () {
            const current = $(this).find('.current');
            const currentText = current.text();


            if (currentText !== 'select') {
                $(this).addClass('is-changed');
            }
        })
    }
}
